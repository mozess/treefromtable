﻿using System;
using System.Data;
using System.Windows.Forms;

namespace TreeFromTable
{
    public partial class TreeFromTable : Form
    {
        public TreeFromTable()
        {
            InitializeComponent();
        }

        private void TreeFromTable_Load(object sender, EventArgs e)
        {
            WorkDB workDB = new WorkDB();
            workDB.CreateDB();
            TreeFilling(workDB.SelectDB("SELECT id, parent_id, name FROM catalog_level ORDER BY id"));
        }
        #region Метод отрисовки TreeView
        /// <summary>
        /// Метод отрисовки TreeView
        /// </summary>
        /// <param name="table">DataTable catalog_level</param>
        private void TreeFilling(DataTable table)
        {
            for (int i = 0; i < table.Rows.Count; i++)
                if (table.Rows[i][1].ToString() == "")
                    tvData.Nodes.Add(table.Rows[i][0].ToString(), table.Rows[i][2].ToString());
                else
                    if (tvData.Nodes.Find(table.Rows[i][1].ToString(), true).Length>0)
                        tvData.Nodes.Find(table.Rows[i][1].ToString(), true)[0].Nodes.Add(table.Rows[i][0].ToString(), table.Rows[i][2].ToString());
            tvData.ExpandAll();
        }
        #endregion
    }
}
