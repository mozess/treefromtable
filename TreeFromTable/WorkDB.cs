﻿using System;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Windows.Forms;

namespace TreeFromTable
{
    class WorkDB
    {
        private static string fileName = "CatalogDB.sdf";
        private static string password = "mypassword";
        private static string connectionString = string.Format($"DataSource=\"{fileName}\"; Password='{password}'");

        # region Метод создания и заполнения БД
        /// <summary>
        /// Метод создания и заполнения БД
        /// </summary>
        public void CreateDB()
        {
            if (File.Exists(fileName))
                return;

            SqlCeEngine sqlCeEngine = new SqlCeEngine(connectionString);
            sqlCeEngine.CreateDatabase();

            #region создание и заполнение данными таблицы catalog
            //создание
            string createCatalog = "CREATE TABLE catalog(id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, name NVARCHAR(100) NOT NULL)";
            SqlCmd(createCatalog);
            //заполнение
            DBInsert("INSERT INTO catalog (name) VALUES ('VOLVO')");
            DBInsert("INSERT INTO catalog (name) VALUES ('ER')");
            #endregion


            #region создание и заполнение данными таблицы aggregate
            //создание
            string createAggregate = "CREATE TABLE aggregate(id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, name NVARCHAR(100) NOT NULL, catalog_id INT NOT NULL)";
            string alterAggregate = "ALTER TABLE aggregate ADD CONSTRAINT FK_CATALOG_ID FOREIGN KEY(catalog_id) REFERENCES catalog(id)";
            SqlCmd(createAggregate);
            SqlCmd(alterAggregate);
            //заполнение          
            DBInsert($"INSERT INTO aggregate (name,catalog_id) VALUES ('КПП', {SelectDB("select id from catalog where name='VOLVO'").Rows[0][0]})");
            DBInsert($"INSERT INTO aggregate (name,catalog_id) VALUES ('Двигатель', {SelectDB("select id from catalog where name='ER'").Rows[0][0]})");
            DBInsert($"INSERT INTO aggregate (name,catalog_id) VALUES ('КПП', {SelectDB("select id from catalog where name='ER'").Rows[0][0]})");
            #endregion


            #region создание и заполнение данными таблицы model
            //создание
            string createModel = "CREATE TABLE model(id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, name NVARCHAR(100) NOT NULL, aggregate_id INT NOT NULL)";
            string alterModel = "ALTER TABLE model ADD CONSTRAINT FK_AGGREGATE_ID FOREIGN KEY(aggregate_id) REFERENCES aggregate(id)";
            SqlCmd(createModel);
            SqlCmd(alterModel);
            //заполнение
            DBInsert($"INSERT INTO model (name, aggregate_id) VALUES ('A365', {SelectDB("SELECT a.id FROM aggregate AS a RIGHT JOIN(SELECT id FROM catalog WHERE name = 'VOLVO') AS t1 ON a.catalog_id = t1.id WHERE a.name = 'КПП'").Rows[0][0]})");
            DBInsert($"INSERT INTO model (name, aggregate_id) VALUES ('M4566', {SelectDB("SELECT a.id FROM aggregate AS a RIGHT JOIN(SELECT id FROM catalog WHERE name = 'ER') AS t1 ON a.catalog_id = t1.id WHERE a.name = 'Двигатель'").Rows[0][0]})");
            DBInsert($"INSERT INTO model (name, aggregate_id) VALUES ('FG4511', {SelectDB("SELECT a.id FROM aggregate AS a RIGHT JOIN(SELECT id FROM catalog WHERE name = 'ER') AS t1 ON a.catalog_id = t1.id WHERE a.name = 'Двигатель'").Rows[0][0]})");
            DBInsert($"INSERT INTO model (name, aggregate_id) VALUES ('T45459', {SelectDB("SELECT a.id FROM aggregate AS a RIGHT JOIN(SELECT id FROM catalog WHERE name = 'ER') AS t1 ON a.catalog_id = t1.id WHERE a.name = 'КПП'").Rows[0][0]})");
            #endregion


            #region создание и заполнение данными таблицы catalog_level
            //создание
            string createCatalogLevel = "CREATE TABLE catalog_level(id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, parent_id INT, name NVARCHAR(100) NOT NULL)";
            SqlCmd(createCatalogLevel);
            //заполнение
            //catalog
            var catalogTable = SelectDB(@"SELECT name
                                          FROM catalog
                                          ORDER BY id");

            for (int i = 0; i < catalogTable.Rows.Count; i++)
                DBInsert($"INSERT INTO catalog_level (name) VALUES ('{catalogTable.Rows[i][0]}')");

            //aggregate
            var aggregateTable = SelectDB(@"SELECT cl.id,
                                                   a.name
                                            FROM aggregate AS a
                                            LEFT JOIN catalog AS c
                                            ON a.catalog_id=c.id
                                            LEFT JOIN catalog_level cl
                                            ON c.name=cl.name
                                            ORDER BY a.id");

            for (int i = 0; i < aggregateTable.Rows.Count; i++)
                DBInsert($"INSERT INTO catalog_level (parent_id,name) VALUES ('{aggregateTable.Rows[i][0]}','{aggregateTable.Rows[i][1]}')");

            //model
            var modelTable = SelectDB(@"SELECT t2.id, 
                                                t1.mod_name
                                         FROM (SELECT c.name AS cat_name,
                                                      a.name AS aggr_name,
                                                      m.id AS mod_id,
                                                      m.name AS mod_name
                                               FROM catalog AS c,
                                                    aggregate AS a,
                                                    model AS m
                                               WHERE 1=1
                                                     AND c.id = a.catalog_id
                                                     AND a.id = m.aggregate_id) AS t1,
                                               (SELECT cl1.name AS cat_name,
                                                       cl2.id AS id,
                                                       cl2.name AS aggr_name
                                               FROM catalog_level cl1,
                                                    catalog_level cl2
                                               WHERE cl1.id = cl2.parent_id) AS t2
                                         WHERE 1=1
                                               AND t1.aggr_name = t2.aggr_name
                                               AND t1.cat_name = t2.cat_name
                                         ORDER BY t1.mod_id");

            for (int i = 0; i < modelTable.Rows.Count; i++)
                DBInsert($"INSERT INTO catalog_level (parent_id,name) VALUES ('{modelTable.Rows[i][0]}','{modelTable.Rows[i][1]}')");

            #endregion
        }
        #endregion

        #region Метод добавления одной строки в БД
        /// <summary>
        /// Метод добавления одной строки в БД (нужно для заполнения БД данными)
        /// </summary>
        /// <param name="insert">Строка запроса insert</param>
        public void DBInsert(string insert)
        {
            var myConnection = new SqlCeConnection(connectionString);
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ПОДКЛЮЧЕНИЕ К БД: " + "\n" + ex.Message);
                return;
            }
            var cmd = myConnection.CreateCommand();
            try
            {
                cmd.CommandText = insert;
                cmd.ExecuteNonQuery();
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ДОБАВЛЕНИИ ДАННЫХ БД: " + "\n" + ex.Message);
                myConnection.Close();
                return;
            }
        }
        #endregion

        #region Метод для команды создания БД
        /// <summary>
        /// Метод для команды создания БД
        /// </summary>
        /// <param name="query">Запрос</param>
        private void SqlCmd(string query)
        {
            SqlCeConnection myConnection = new SqlCeConnection(connectionString);
            SqlCeCommand cmd = new SqlCeCommand(query, myConnection);
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ПОДКЛЮЧЕНИЕ К БД: " + "\n" + ex.Message);
                return;
            }
            try
            {
                cmd.ExecuteNonQuery();
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ РАБОТЕ С БД: " + "\n" + ex.Message);
                myConnection.Close();
                return;
            }
        }
        #endregion

        #region Метод получения данных из БД
        /// <summary>
        /// Метод получения данных из БД
        /// </summary>
        /// <param name="select">строка Select</param>
        /// <returns></returns>
        public DataTable SelectDB(string select)
        {
            var sqlCeConnection = new SqlCeConnection(connectionString);
            var dataAdapter = new SqlCeDataAdapter(select, sqlCeConnection);
            var commandBuilder = new SqlCeCommandBuilder(dataAdapter);
            var dataSet = new DataSet();
            try
            {
                dataAdapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Возникла проблема при подключение к бд: " + ex.Message);
                return null;
            }
            return dataSet.Tables[0];
        }
        #endregion

    }
}
